# OSAS GitHub Settings

## Introduction

It is boring to click in the interface on each repository to change
settings. There is no way to be sure a new one is properly setup or
there is no inconsistency.

This script is meant to be run regularly (using cron) and enforce
the OSAS policy on repositories we manage.

Currently the script is able to list organisations the user is a
member of and check if the default branch (often `master`, but can
be setup per-repository according to the project's needs) is
protected with the wanted policy. It is not yet able to update it
because the GitHub API in this area is WIP.

## Setup

### Tool Setup

The `gh-settings.yml` configuration file contains the following
settings:

  - ignore_repos: list of regexes of repo full names you wish to be
      ignored by this script
  - select_repos: list of regexes of repo full names you wish to be
      taken care of by this script
  - wanted_protection: the policy parameters you want to enforce for
      the default branch; see example configuration file for possible
      parameters

Ignored repositories (`ignore_repos`) are handled first, then if set
the remaining list is filtered by the wanted selection
(`select_repos`).

### GitHub Setup

Go to the settings of the GitHub account you want to use, in the
*Personal access tokens* tab, then create a new token. Use the
name of your choice, and give the following rights:

  - repo

Keep the generated token safe. You need to export it in the
`GITHUB_TOKEN` environment variable before running this tool.

